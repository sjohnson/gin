#include "GinManager.h"
#include <sstream>
#include <algorithm>
#include <numeric>
#include <iostream>

using namespace std;

GinManager::GinManager() { }

//Public Methods

// Background
// ===========
// The goal is to compute all possible hands of 10 cards which make a gin.
// This is accomplished by having all 10 cards be part of a trick.
// There are 5 categories of trick:
//     3-of-a-kind,
//     4-of-a-kind,
//     3-card-straight,
//     4-card-straight,
//     5-card-straight
// and so a trick can only be made of 3, 4, or 5 cards. This implies that a
// gin hand can only be achieved with two 5-card-straight (5-5), or a combination
// of a 4-trick, and two 3-tricks (4-3-3)
//
// Description of Algorithm
// ========================
// First we compute all of the possible tricks, which is relatively straightforward.
// We then iterate through these lists of tricks to produce potential 5-5 and 4-3-3 hands.
// Some tricks can overlap, so cannot exist in the same hand (eg, the tricks AH AD AS AC and AH 2H 3H)
// so for every combination, we check if the tricks form a valid hand by looking for duplicate cards
//
// One gotcha is that one hand can make gin in multiple ways, so by iterating over groups of tricks,
// we may form the same hand multiple times. To avoid counting hands more than once, we keep track of hands we have counted by
// inserting them into an unordered_set, and defining an appropriate hash on the hands (CardSet)
unordered_set<CardSet> GinManager::ComputeGinHands() {
    vector<int> cards(52);
    iota(begin(cards), end(cards), 1);
    return ComputeGinHands(cards);
}

unordered_set<CardSet> GinManager::ComputeGinHands(vector<int>& cards) {
    SetUsableCards(cards);

    //use unordered_set to avoid counting duplicate hands
    unordered_set<CardSet> result;

    //compute 5-5 tricks (find all pairs, and avoid double counting by setting i < j)
    for (int j = 1; j < trick_list.fives.size(); ++j) {
        for (int i = 0; i < j; ++i) {
            CardSet card_set;
            card_set.Insert(trick_list.fives[i]);
            card_set.Insert(trick_list.fives[j]);

            if (!card_set.HasDuplicates()) {
                result.insert(card_set);
            }
        }
    }

    //compute 4-3-3 tricks
    //(group every four with all pairs of threes, and avoid double counting of threes as before)
    for (auto& four_trick : trick_list.fours) {
        for (int j = 1; j < trick_list.threes.size(); ++j) {
            for (int i = 0; i < j; ++i) {
                CardSet card_set;
                card_set.Insert(trick_list.threes[i]);
                card_set.Insert(trick_list.threes[j]);
                card_set.Insert(four_trick);
                if (!card_set.HasDuplicates()) {
                    result.insert(card_set);
                }
            }
        }
    }
    return result; 
}

bool GinManager::IsGin(vector<int> hand) {
    auto x = ComputeGinHands(hand);
    return x.size();
}

//Static Functions

const char GinManager::Face(const int card) {
    int f = 1 + (card - 1) % 13;
    if (f ==  1) return 'A';
    else if (f == 10) return 'T';
    else if (f == 11) return 'J';
    else if (f == 12) return 'Q';
    else if (f == 13) return 'K';
    else return (f + '0');
}

const char GinManager::Suit(const int card) {
    int s = (card - 1) / 13;
    if (s == 0) return 'C';
    else if (s == 1) return 'D';
    else if (s == 2) return 'H';
    else if (s == 3) return 'S';
    else return 'X';
}

string GinManager::card_to_string(const int card) {
    ostringstream stream;
    stream << Face(card) << Suit(card);
    return stream.str();
}

string GinManager::cards_to_string(CardSet& cards) {
    ostringstream stream;
    for (int i = 0; i <= 52; ++i) {
        int c = cards.card_counts[i];
        if (c) {
            stream << card_to_string(i) << " ";
        }
    }
    return stream.str();
}

string GinManager::cards_to_string(vector<int>& cards) {
    ostringstream stream;
    for (int c : cards) {
        stream << card_to_string(c) << " ";
    }
    return stream.str();
}

const int GinManager::string_to_card(const std::string card) {
    if (card.size() != 2) return -1;
    int face;
    if (card[0] == 'A') face = 1;
    else if (card[0] == 'T') face = 10;
    else if (card[0] == 'J') face = 11;
    else if (card[0] == 'Q') face = 12;
    else if (card[0] == 'K') face = 13;
    else face = card[0] - '0';

    int suit = -1;
    if (card[1] == 'C') suit = 0;
    else if (card[1] == 'D') suit = 1;
    else if (card[1] == 'H') suit = 2;
    else if (card[1] == 'S') suit = 3;

    return face + suit * 13;
}

//Private Methods

//Sets which cards are usable for constructing tricks.
//Reconstruct the trick list
void GinManager::SetUsableCards(vector<int>& cards) {
    memset(usable_cards, 0, 53 * sizeof(int));
    for (int c : cards) {
        usable_cards[c] = 1;
    }

    //compute tricks from usable_cards
    trick_list.clear();

    //compute the fives (all of these are straights)
    for (int suit = 0; suit < 4; ++suit) {
        int base = suit * 13;
        for (int i = 1; i <= 9; ++i) {
            int j = base + i;
            if (usable_cards[j] &&
                usable_cards[j + 1] &&
                usable_cards[j + 2] &&
                usable_cards[j + 3] &&
                usable_cards[j + 4]) {
                vector<int> trick = {j, j + 1, j + 2, j + 3, j + 4};
                trick_list.insert(trick);
            }
        }
    }

    //compute the fours
    //first the four-of-a-kinds
    for (int i = 1; i <= 13; ++i) {
        if (usable_cards[i] && usable_cards[i + 13] && usable_cards[i + 2*13] && usable_cards[i + 3*13]) {
            vector<int> trick = {i, i + 13, i + 2*13, i + 3*13};
            trick_list.insert(trick);
        }
    }
    //next, the straights
    for (int suit = 0; suit < 4; ++suit) {
        int base = suit * 13;
        for (int i = 1; i <= 10; ++i) {
            int j = base + i;
            if (usable_cards[j] &&
                usable_cards[j + 1] &&
                usable_cards[j + 2] &&
                usable_cards[j + 3]) {
                vector<int> trick = {j, j + 1, j + 2, j + 3};
                trick_list.insert(trick);
            }
        }
    }
    //compute the threes
    for (int i = 1; i <= 13; ++i) {
        if (usable_cards[i] && usable_cards[i + 13] && usable_cards[i + 2*13]) {
            vector<int> trick = {i, i + 13, i + 2*13};
            trick_list.insert(trick);
        }
    }
    //next, the straights
    for (int suit = 0; suit < 4; ++suit) {
        int base = suit * 13;
        for (int i = 1; i <= 11; ++i) {
            int j = base + i;
            if (usable_cards[j] &&
                usable_cards[j + 1] &&
                usable_cards[j + 2]) {
                vector<int> trick = {j, j + 1, j + 2};
                trick_list.insert(trick);
            }
        }
    }
}
