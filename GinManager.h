#pragma once
#include <vector>
#include <unordered_set>
#include "CardSet.h"
#include "TrickList.h"

class GinManager {

public:
    GinManager();
    ~GinManager() {}

    //Compute the number of hands that can be dealt using all 52 cards
    //that result in gin
    std::unordered_set<CardSet> ComputeGinHands();

    //Compute the number of hands that can be dealt using cards
    //that result in gin
    std::unordered_set<CardSet> ComputeGinHands(std::vector<int>& cards); 

    //Compute if gin is possible with the 10 cards in hand,
    //using only tricks made of cards from usable_cards
    bool IsGin(std::vector<int> hand);

    //static helper functions

    //Cards are numbers from 1 to 52. Return the corresponding
    //2 letter code. Eg 1 -> AC, 2 -> 2C .. 14 -> AD
    static std::string card_to_string(const int card);
    static std::string cards_to_string(CardSet& cards);
    static std::string cards_to_string(std::vector<int>& cards);

    //Return the 1 letter face code for the given card number
    static const char Face(const int card);

    //Return the 1 letter suit code for the given card number
    static const char Suit(const int card);

    //Convert the 2 letter code to the correct card number
    static const int string_to_card(const std::string card);

private:
    //Sets usable_cards to cards
    void SetUsableCards(std::vector<int>& cards);
    int usable_cards[53];
    TrickList trick_list;
};
