project (is_gin CXX)
cmake_minimum_required (VERSION 2.8.11)
add_executable (IsGin IsGin.cpp TrickList.cpp CardSet.cpp GinManager.cpp)
set_property(TARGET IsGin PROPERTY CXX_STANDARD 14)
set_property(TARGET IsGin PROPERTY CXX_STANDARD_REQUIRED ON)

add_executable (ComputeGinHands ComputeGinHands.cpp TrickList.cpp CardSet.cpp GinManager.cpp)
set_property(TARGET ComputeGinHands PROPERTY CXX_STANDARD 14)
set_property(TARGET ComputeGinHands PROPERTY CXX_STANDARD_REQUIRED ON)
